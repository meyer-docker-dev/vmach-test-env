FROM alpine

RUN apk update && apk add --no-cache openjdk8 nodejs npm maven curl bash

# Installing leiningen and clojure

RUN wget https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein -O /bin/lein && \
    chmod +x /bin/lein && lein && \
    cd /tmp && wget https://download.clojure.org/install/linux-install-1.10.1.447.sh && \
    chmod +x linux-install-1.10.1.447.sh && ./linux-install-1.10.1.447.sh
    

COPY dummy /tmp/dummy

RUN cd /tmp/dummy && lein deps